# External Minimap
A laggy dot minimap accessible from a webpage showing player positions in game within the last second on a square minimap, only a prototype so the "map" is just a square gray void.